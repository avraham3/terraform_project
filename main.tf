# Resource group
resource "azurerm_resource_group" "terraform_gifts_rg" {
  name     = "${var.resource_group_name}${var.project_name}"
  location = "West Europe"

}

#virtual network
resource "azurerm_virtual_network" "terraform_gifts_vnet" {
  name                = "terraform-gifts-WestEurope-vnet"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  address_space       = ["10.0.0.0/16"]
}

# subnet for web host 
resource "azurerm_subnet" "web_subnet" {
  virtual_network_name = azurerm_virtual_network.terraform_gifts_vnet.name
  name                 = "web-subnet"
  resource_group_name  = azurerm_resource_group.terraform_gifts_rg.name
  address_prefixes     = ["10.0.1.0/24"]
}


# subnet for db host 
resource "azurerm_subnet" "db_subnet" {
  virtual_network_name = azurerm_virtual_network.terraform_gifts_vnet.name
  name                 = "db-subnet"
  resource_group_name  = azurerm_resource_group.terraform_gifts_rg.name
  address_prefixes     = ["10.0.2.0/24"]
}

# network security group for web
resource "azurerm_network_security_group" "nsg_gifts_web_dev" {
  name                = "nsg-gifts-web-dev"
  location            = azurerm_resource_group.terraform_gifts_rg.location
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name

  security_rule {
    name                       = "AllowPortsCustomInbound"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }

  security_rule {
    name                       = "Allow_Web_Port_80"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }


  security_rule {
    name                       = "Allow_Web_Port_5000"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5000"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }




  security_rule {
    name                       = "AllowCustominbound"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*" # only my public ip adress
    destination_address_prefix = "*"
  }
}

#associate the network security group with the web subnet
resource "azurerm_subnet_network_security_group_association" "web_nsg_association" {
  subnet_id                 = azurerm_subnet.web_subnet.id
  network_security_group_id = azurerm_network_security_group.nsg_gifts_web_dev.id
}

#network security group for db
resource "azurerm_network_security_group" "nsg_gifts_db_dev" {
  name                = "nsg-gifts-db-dev"
  location            = azurerm_resource_group.terraform_gifts_rg.location
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name

  security_rule {
    name                       = "AllowCidrBlockCustomInbound"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5432"
    source_address_prefix      = "10.0.1.0/24"
    destination_address_prefix = "*"
  }


  security_rule {
    name                       = "AllowCustominbound"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*" # only my public ip adress
    destination_address_prefix = "*"
  }
}

#associate the network security group with the db subnet
resource "azurerm_subnet_network_security_group_association" "db_nsg_association" {
  subnet_id                 = azurerm_subnet.db_subnet.id
  network_security_group_id = azurerm_network_security_group.nsg_gifts_db_dev.id
}

#public ip for web host 
resource "azurerm_public_ip" "web_public_ip" {
  name                = "web-public-ip"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  allocation_method   = "Static"
}

#network interface for web host
resource "azurerm_network_interface" "web_nic" {
  name                = "web_nic"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  ip_configuration {
    name                          = "web-ip-config"
    subnet_id                     = azurerm_subnet.web_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.web_public_ip.id

  }
}

#public ip for postgreSQL db
resource "azurerm_public_ip" "db_public_ip" {
  name                = "db_public_ip"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  allocation_method   = "Static"
}

# network interface for postgreSQL db
resource "azurerm_network_interface" "db_nic" {
  name                = "db-nic"
  resource_group_name = azurerm_resource_group.terraform_gifts_rg.name
  location            = azurerm_resource_group.terraform_gifts_rg.location
  ip_configuration {
    name                          = "ipconfig-db-private"
    subnet_id                     = azurerm_subnet.db_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.0.2.10"
    primary                       = true
  }


  ip_configuration {
    name                          = "db-ip-config"
    subnet_id                     = azurerm_subnet.db_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.db_public_ip.id

  }


}
# VM for the web host
resource "azurerm_virtual_machine" "web_vm" {
  name                  = "web-vm"
  resource_group_name   = azurerm_resource_group.terraform_gifts_rg.name
  location              = azurerm_resource_group.terraform_gifts_rg.location
  vm_size               = "Standard_B1s"
  network_interface_ids = [azurerm_network_interface.web_nic.id]

  storage_os_disk {
    name              = "web-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "webhost"
    admin_username = "adminuser"
    admin_password = var.Password
  }

  plan {
    publisher = "Canonical"
    product   = "UbuntuServer"
    name      = "18.04-LTS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

# VM for the PostgreSQL DB
resource "azurerm_virtual_machine" "db_vm" {
  name                  = "db-vm"
  resource_group_name   = azurerm_resource_group.terraform_gifts_rg.name
  location              = azurerm_resource_group.terraform_gifts_rg.location
  vm_size               = "Standard_B2s"
  network_interface_ids = [azurerm_network_interface.db_nic.id]

  storage_os_disk {
    name              = "db-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "dbhost"
    admin_username = "adminuser"
    admin_password = var.Password
  }

  plan {
    publisher = "Canonical"
    product   = "UbuntuServer"
    name      = "18.04-LTS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}
#create db vm managed disk
resource "azurerm_managed_disk" "db-disk" {
  name                 = "${azurerm_virtual_machine.db_vm.name}-disk1"
  location             = azurerm_resource_group.terraform_gifts_rg.location
  resource_group_name  = azurerm_resource_group.terraform_gifts_rg.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 4
}
#attach db disk to web vm
resource "azurerm_virtual_machine_data_disk_attachment" "db_disk_attach" {
  managed_disk_id    = azurerm_managed_disk.db-disk.id
  virtual_machine_id =azurerm_virtual_machine.db_vm.id
  lun                = "4"
  caching            = "ReadWrite"
}
resource "azurerm_virtual_machine" "web_vm" {
  # ... (your existing configuration)

  provisioner "remote-exec" {
    inline = [
      "echo 'Hello, World!' > /home/adminuser/hello.txt"
    ]

    connection {
      type     = "ssh"
      user     = "adminuser"
      password = var.Password
      host     = azurerm_public_ip.web_public_ip.ip_address
    }
  }
}
resource "azurerm_virtual_machine" "db_vm" {
  # ... (your existing configuration)

  provisioner "remote-exec" {
    inline = [
      "echo 'Setting up the DB!' > /home/adminuser/db_setup.txt"
    ]

    connection {
      type     = "ssh"
      user     = "adminuser"
      password = var.Password
      host     = azurerm_public_ip.db_public_ip.ip_address
    }
  }
}
resource "azurerm_virtual_machine_extension" "web_ext" {
  name                 = "install_flask"
  virtual_machine_id   = azurerm_linux_virtual_machine.vm-web.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.1"

  settings = <<SETTINGS
{
  "commandToExecute": "sudo apt-get update && sudo apt install git -y && git clone ${var.git_repo} && sudo bash ./avraham3/terraform_project/-/blob/main/web_script.bash/ '${var.app_port}' '${var.db_ip}' '${var.db_user}' '${var.db_password}' '${var.web_subnet}'"
}
SETTINGS

  depends_on = [
  azurerm_linux_virtual_machine.vm-db,
    azurerm_virtual_machine_extension.db_ext,
    azurerm_linux_virtual_machine.vm-web
  ]
}

#creating db extension
resource "azurerm_virtual_machine_extension" "db_ext" {
  name                 = "install_flask"
  virtual_machine_id   = azurerm_linux_virtual_machine.vm-db.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.1"

  settings = <<SETTINGS
{
  "commandToExecute": "sudo apt-get update && sudo apt install git -y && git clone ${var.git_repo} && sudo bash./avraham3/terraform_project/-/blob/main/db_script.bash/'${var.app_port}' '${var.db_ip}' '${var.db_user}' '${var.db_password}' '${var.web_subnet}' "
}
SETTINGS

  depends_on = [
  azurerm_linux_virtual_machine.vm-db
  ]
}